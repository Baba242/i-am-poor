import 'package:flutter/material.dart';

void main()=>runApp(
  MaterialApp(
    home:  Scaffold(
      backgroundColor: Colors.blueAccent,
      appBar: AppBar(
        title: Text("I Am Poor"),
        backgroundColor: Colors.blueGrey[900],
      ),
      body: Center(
        child: Image.asset("images/poor.png"),
      ),
    ),
  ),
);